import Vue from 'vue'
import App from './App'
import toPage from "utils/toPage"
import ShopItem from "@/components/shopitem"
import '@/utils/changeShopIconNumber'
import baseImage from './components/baseImage';

Vue.config.productionTip = false
App.mpType = 'app'
Vue.prototype.$toPage = toPage
Vue.component('shop-item',ShopItem);
Vue.component('base-image',baseImage);
// 初始化云开发
wx.cloud.init({
  env: 'min-gucof',
  traceUser: true,
})
// 新建实例
async function createCloud(){
  const o_cloud = new wx.cloud.Cloud({
    resourceEnv: 'min-gucof',
    traceUser: true,
  })
  await o_cloud.init()
  return o_cloud;
}
Vue.prototype.$cloud = createCloud();
const app = new Vue({
  ...App
})
app.$mount()