import http from "@/utils/httpTool";
export const requestShopCar = () => http.post('/p/shopCart/info');
//获取件数
export const requestGetShopCarCount = () => http.get('/p/shopCart/prodCount');
//价格
export const requestReducePrice = (data) => http.post('/p/shopCart/totalPay', data);
//数量加减
export const requestChangeItem  = (data) => http.post('/p/shopCart/changeItem', data);
//删除
export const requestDeleteItem  = (data) => http.delete('/p/shopCart/deleteItem', data);
