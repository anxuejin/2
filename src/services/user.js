import http from '../utils/httpTool';
export const login = (data) => http.post('/login', data)
export const gitCartList = (data) => http.post('/p/shopCart/info', data)
export const prodTagList = (data) => http.get('/prod/tag/prodTagList', data);
//每日上新更多数据
export const prodList = (data) => http.get('/prod/prodListByTagId?tagId=1&current=1&size=10', data);

// 订单详情
export const orderList = (data) => http.get('/p/myOrder/orderDetail', data);
export const orderCount = (data) => http.get('/p/myOrder/orderCount', data);

// 订单列表数据
export const getorderList = (data) => http.get('/p/myOrder/myOrder', data)

// 我的页面数据
export const getmyList = (data) => http.get('/p/myOrder/orderCount', data)

export const carouselIndex = (data) => http.get('/indexImgs', data)
//首页商品详情
export const prodListDetail = (data) => http.get('/prod/prodInfo', data)
//新品推荐数据
export const lastedProd = (data) => http.get('/prod/lastedProdPage?current=1&size=10', data)
//限时特惠
export const discountProd = (data) => http.get('/prod/discountProdList?current=1&size=10', data)
//每日疯抢
export const moreBuyProd = (data) => http.get('/prod/moreBuyProdList?current=1&size=10', data)
//最新公告数据
export const topNoticeList = (data) => http.get('/shop/notice/topNoticeList', data)
export const newsList = (id) => http.get(`/shop/notice/info/${id}`, id)
//每日上新数据
export const dayNew = (data) => http.get('/prod/prodListByTagId?tagId=1&size=6', data)
export const dayNew2 = (data) => http.get('/prod/prodListByTagId?tagId=3&size=6', data)
//分类页数据
export const category = (data) => http.get('/category/categoryInfo?parentId=', data)
export const product = (data) => http.get('/prod/pageProd', data)
//分类页跳详情
export const toDetail = (id) => http.get('/prod/prodInfo?prodId=', id)

// 收货地址数据
export const addressList = (data) => http.get('/p/address/list', data)
//授权接口
export const setUserInfo = (data) => http.put('/p/user/setUserInfo', data)
// 设置默认地址
export const defaultAddr = (id) => http.put(`/p/address/defaultAddr/${id}`)
// 编辑收货地址回显
export const addrInfo = (id) => http.get(`/p/address/addrInfo/${id}`)
// 删除收货地址
export const delAddress = (id) => http.delete(`/p/address/deleteAddr/${id}`)
// 编辑收货地址
//保存收货地址
export const updateAddr = (data) => http.put('/p/address/updateAddr',data)
//添加收货地址
export const addAddr = (data) => http.post('/p/address/addAddr',data)
//首页商品详情加入购物车
export const addShopCar = (data) => http.post('/p/shopCart/changeItem', data)
//提交订单
export const shopListSubmit = (data) => http.post('/p/order/confirm', data)
// 我的收藏数量
export const getCount = () => http.get('/p/user/collection/count')
// 我的收藏商品数据
export const collectionList = (data) => http.get('/p/user/collection/prods',data)
//提交订单
export const submit=(data)=>http.post('/p/order/submit',data)
//pay
export const pay=(data)=>http.post('/p/order/pay',data)

// 我的收藏商品数据
export const getcode = (data) => http.post('/p/sms/send', data)
export const getCancel = (id) => http.put(`/p/myOrder/cancel/${id}`)
// export const collectionList = (data) => http.get('/p/user/collection/prods',data)
// 搜索结果数据
export const prodPageList = (data) => http.get('/search/searchProdPage',data)
//点击收藏
export const collection = (data) => http.post('/p/user/collection/addOrCancel',data);
