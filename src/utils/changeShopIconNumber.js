import { requestGetShopCarCount } from "@/services/shopcar"
import Vue from "vue";
const changeShopIconNumber = async function () {
  try {
    const count = await requestGetShopCarCount();
    count > 0 &&  uni.setTabBarBadge({
      index: 3,
      text: `${count}`
    })
  } catch (error) {
    console.log('error', error);
  }
}

Vue.prototype.changeShopIconNumber = changeShopIconNumber;

export default changeShopIconNumber;
