export const formatUrlQueryString = (baseUrl,query) => {
    return Object.keys(query).length ? (baseUrl + "?" + Object.keys(query).map(key=>`${key}=${query[key]}`).join('&')) : baseUrl;
}

export default function ({url='',query={},redirect=false,tabbar=false}) 
{
    //处理query 拼接url
    url = formatUrlQueryString(url, query);
    // console.log(url);
    //如果跳转的页面是tabbar页面 使用switch跳转
    const methodName = tabbar ? 'switchTab' : redirect ? "redirectTo" : "navigateTo";
    return new Promise((resolve, reject) => {
        uni[methodName]({
            url,
            success() 
            {
                resolve()
            },
            fail(error) 
            {
                reject(error);
            }
        })
    })
    //如果redirect值为true 使用redirect跳转
    //以上两个条件都不符合使用navigateTo跳转并且解决十次的问题
}